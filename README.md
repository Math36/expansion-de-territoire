# IA04 - Projet

Membres :

- Penverne Leonard
- Garnier Kilian
- Kerjean Victor
- Joubin Hippolyte
- Vast Mathias

Dans le cadre d'un projet scolaire, nous devions mettre en place un système multi-agent. Nous avons fait le choix de simuler l'extension de nation sur une carte contenant des ressources pouvant être exploitées par les villes de chaque nation.

Technologies : Java / Plateforme MASON

-------------

Members : 

- Penverne Leonard
- Garnier Kilian
- Kerjean Victor
- Joubin Hippolyte
- Vast Mathias

For an academic project, we had to set up a multi-agent system. We made the choice to simulate the expansion of several nations on a map. The map also contains ressources that can be used by the the cities of the nations.

Technologies : Java / MASON platform
